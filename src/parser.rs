use crate::lexer::C1Token;
use crate::lexer::C1Lexer;

impl std::fmt::Display for C1Token {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        write!(f, "{:?}", self)
    }
}

pub struct C1Parser<'a> {
	lexer: C1Lexer<'a>
}

impl C1Parser<'_> {
	pub fn parse(text: &str) -> Result<(), String> {
		let mut parser : C1Parser = C1Parser {
			lexer : C1Lexer::new(text)
		};
        return parser.program();
	}
	fn program(&mut self) -> Result<(), String> {
        while self.lexer.current_token().is_some() {
			self.functiondefinition().unwrap();
        }
		return Ok(());
	}

	fn eat_and_check(&mut self, expected_token: C1Token) -> Result<(), String> {
		match self.lexer.current_token() {
			Some(token) => {
				if token == expected_token {
					self.lexer.eat();
					return Ok(())
				} else {
					return Err(String::from("Unexpected token ") + &token.to_string() + " in line: "+&self.lexer.current_line_number().unwrap().to_string() + ". Expected " + &expected_token.to_string())
				}
			},
            None => return Err(String::from("Unexpected EOF, expected ") + &expected_token.to_string()),
		}
	}

	fn functiondefinition(&mut self) -> Result<(), String> {
        //functiondefinition  ::= type <ID> "(" ")" "{" statementlist "}"
		self._type().unwrap();
        self.eat_and_check(C1Token::Identifier).unwrap();
        self.eat_and_check(C1Token::LeftParenthesis).unwrap();
        self.eat_and_check(C1Token::RightParenthesis).unwrap();
        self.eat_and_check(C1Token::LeftBrace).unwrap();
        self.statementlist().unwrap();
        self.eat_and_check(C1Token::RightBrace).unwrap();
		return Ok(());
	}

	fn functioncall(&mut self) -> Result<(), String> {
        //functioncall        ::= <ID> "(" ")"
		self.eat_and_check(C1Token::Identifier).unwrap();
        self.eat_and_check(C1Token::LeftParenthesis).unwrap();
        self.eat_and_check(C1Token::RightParenthesis).unwrap();
		return Ok(());
	}

	fn statementlist(&mut self) -> Result<(), String> {
        //statementlist       ::= ( block )*
		// block beginnt mit "{" oder statement, statement beginnt mit if, return, printf, identifier
		loop {
			match self.lexer.current_token() {
				Some(C1Token::LeftBrace)|
				Some(C1Token::KwIf)|
				Some(C1Token::KwReturn)|
				Some(C1Token::KwPrintf)|
				Some(C1Token::Identifier) => self.block().unwrap(),

				_ => return Ok(()),
			}
		}
	}

	fn block(&mut self) -> Result<(), String> {
		//block               ::= "{" statementlist "}"
		//                        | statement
		if self.lexer.current_token() == Some(C1Token::LeftBrace) {
			self.lexer.eat();
			self.statement().unwrap();
			self.eat_and_check(C1Token::RightBrace).unwrap();
		} else {
			self.statement().unwrap();
		}
		return Ok(());
	}

	fn statement(&mut self) -> Result<(), String> {
		//statement           ::= ifstatement
        //                     | returnstatement ";"
        //                     | printf ";"
        //                     | statassignment ";"
        //                     | functioncall ";"

		match self.lexer.current_token() {
            Some(C1Token::KwIf) => {return self.ifstatement();},
            Some(C1Token::KwReturn) => {
				self.returnstatement().unwrap();
				self.eat_and_check(C1Token::Semicolon).unwrap();
				return Ok(());
			},
            Some(C1Token::KwPrintf) => {
				self.printf().unwrap();
				self.eat_and_check(C1Token::Semicolon).unwrap();
				return Ok(());
			},
            Some(C1Token::Identifier) => {
                match self.lexer.peek_token() {
                    Some(C1Token::LeftParenthesis) => self.functioncall().unwrap(),
                    _ => self.statassignment().unwrap(),
                }
				self.eat_and_check(C1Token::Semicolon).unwrap();
				return Ok(());
			},
			Some(other) => return Err(String::from("Unexpected token ") + &other.to_string() + " in line: "+&self.lexer.current_line_number().unwrap().to_string() + ". Expected statement"),
            None => return Err(String::from("Unexpected EOF, expected statement")),
		}
	}

	fn ifstatement(&mut self) -> Result<(), String> {
		// ifstatement         ::= <KW_IF> "(" assignment ")" block
		self.eat_and_check(C1Token::KwIf).unwrap();
		self.eat_and_check(C1Token::LeftParenthesis).unwrap();
		self.assignment().unwrap();
		self.eat_and_check(C1Token::RightParenthesis).unwrap();
		self.block().unwrap();
		return Ok(());
	}

	fn returnstatement(&mut self) -> Result<(), String> {
		// returnstatement     ::= <KW_RETURN> ( assignment )?
		self.eat_and_check(C1Token::KwReturn).unwrap();
        match self.lexer.current_token() {
			Some(C1Token::Minus)|
			Some(C1Token::ConstInt)|
			Some(C1Token::ConstFloat)|
			Some(C1Token::ConstBoolean)|
            Some(C1Token::Identifier)|
			Some(C1Token::LeftParenthesis) => {
				self.assignment().unwrap();
			},
			_ => {},
		}
        return Ok(());
	}

	fn printf(&mut self) -> Result<(), String> {
		//printf              ::= <KW_PRINTF> "(" assignment ")"
		self.eat_and_check(C1Token::KwPrintf).unwrap();
		self.eat_and_check(C1Token::LeftParenthesis).unwrap();
		self.assignment().unwrap();
		self.eat_and_check(C1Token::RightParenthesis).unwrap();
		return Ok(());
	}

	fn _type(&mut self) -> Result<(), String> {
		//type                ::= <KW_BOOLEAN>
        //              | <KW_FLOAT>
        //              | <KW_INT>
        //              | <KW_VOID>

		match self.lexer.current_token() {
			Some(C1Token::KwBoolean)|
			Some(C1Token::KwFloat)|
			Some(C1Token::KwInt)|
			Some(C1Token::KwVoid) => {
				self.lexer.eat();
				return Ok(());
			},

			Some(other) => return Err(String::from("Unexpected token ") + &other.to_string() + " in line: "+&self.lexer.current_line_number().unwrap().to_string() + ". Expected type"),
            None => return Err(String::from("Unexpected EOF, expected type")),
		}
	}

	fn statassignment(&mut self) -> Result<(), String> {
		//statassignment      ::= <ID> "=" assignment
		self.eat_and_check(C1Token::Identifier).unwrap();
		self.eat_and_check(C1Token::Assign).unwrap();
		self.assignment().unwrap();
		return Ok(());
	}

	fn assignment(&mut self) -> Result<(), String> {
		// assignment          ::= ( ( <ID> "=" assignment ) | expr )
		// beginnt mit			<ID>"=" | expr
		// beginnt mit			<ID>"=" | simpexpr
		// beginnt mit			<ID>"=" | "-" | term
		// beginnt mit			<ID>"=" | "-" | factor
		// beginnt mit			<ID>"=" | "-" | <CONST_INT> | <CONST_FLOAT> | <CONST_BOOLEAN> | functioncall | <ID> | "("
		// beginnt mit			<ID>"=" | "-" | <CONST_INT> | <CONST_FLOAT> | <CONST_BOOLEAN> | <ID> | "("
		match self.lexer.current_token() {
			Some(C1Token::Minus)|
			Some(C1Token::ConstInt)|
			Some(C1Token::ConstFloat)|
			Some(C1Token::ConstBoolean)|
			Some(C1Token::LeftParenthesis) => {
				self.expr().unwrap();
				return Ok(());
			},
			Some(C1Token::Identifier) => {
				match self.lexer.peek_token() {
					Some(C1Token::Assign) => {
						self.lexer.eat();
						self.lexer.eat(); // Extra hungry
						self.assignment().unwrap();
						return Ok(());
					},
					_ => {
						self.expr().unwrap();
						return Ok(());
					},
				}
			}
			
			Some(other) => return Err(String::from("Unexpected token ") + &other.to_string() + " in line: "+&self.lexer.current_line_number().unwrap().to_string() + ". Expected assignment"),
            None => return Err(String::from("Unexpected EOF, expected assignment")),
		}
	}

	fn expr(&mut self) -> Result<(), String> {
		// expr                ::= simpexpr ( ( "==" | "!=" | "<=" | ">=" | "<" | ">" ) simpexpr )?
		self.simpexpr().unwrap();
		match self.lexer.current_token() {
			Some(C1Token::Equal)|
			Some(C1Token::NotEqual)|
			Some(C1Token::Less)|
			Some(C1Token::LessEqual)|
			Some(C1Token::Greater)|
			Some(C1Token::GreaterEqual) => {
				self.lexer.eat();
				self.simpexpr().unwrap();
			},
			_ => {}
		}
		return Ok(());
	}

	fn simpexpr(&mut self) -> Result<(), String> {
		//simpexpr            ::= ( "-" )? term ( ( "+" | "-" | "||" ) term )*
		if self.lexer.current_token() == Some (C1Token::Minus) {
            self.lexer.eat();
        }
        self.term().unwrap();
        loop {
			match self.lexer.current_token() {
				Some(C1Token::Plus)|
				Some(C1Token::Minus)|
				Some(C1Token::Or) => {
                    self.lexer.eat();
                    self.term().unwrap();
                },
				_ => return Ok(()),
			}
		}
	}

	fn term(&mut self) -> Result<(), String> {
		// term                ::= factor ( ( "*" | "/" | "&&" ) factor )*
		self.factor().unwrap();
        loop {
			match self.lexer.current_token() {
				Some(C1Token::Asterisk)|
				Some(C1Token::Slash)|
				Some(C1Token::And) => {
                    self.lexer.eat();
                    self.factor().unwrap();
                },
				_ => return Ok(()),
			}
		}
	}



	fn factor(&mut self) -> Result<(), String> {
		//factor              ::= <CONST_INT>
        //              | <CONST_FLOAT>
        //              | <CONST_BOOLEAN>
        //              | functioncall
        //              | <ID>
        //              | "(" assignment ")"

		match self.lexer.current_token() {
            Some(C1Token::ConstInt) => {self.lexer.eat(); return Ok(())},
            Some(C1Token::ConstFloat) => {self.lexer.eat(); return Ok(())},
            Some(C1Token::ConstBoolean) => {self.lexer.eat(); return Ok(())},
            Some(C1Token::LeftParenthesis) => {
				self.lexer.eat(); self.assignment().unwrap(); 
                self.eat_and_check(C1Token::RightParenthesis).unwrap(); return Ok(())
			},
            Some(C1Token::Identifier) => {
                match self.lexer.peek_token() {
                    Some(C1Token::LeftParenthesis) => self.functioncall().unwrap(),
                    _ => self.lexer.eat(),
                }
                return Ok(())
            },
			Some(other) => return Err(String::from("Unexpected token ") + &other.to_string() + " in line: "+&self.lexer.current_line_number().unwrap().to_string() + ". Expected factor"),
            None => return Err(String::from("Unexpected EOF, expected factor")),
		}
	}

    
}